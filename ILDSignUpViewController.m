//
//  ILDSignUpViewController.m
//  iLambda
//  Copyright © 2014, First Quadrant Solutions Pvt. Ltd.
//  Written under contract by Robosoft Technologies Pvt. Ltd



#import "ILDSignUpViewController.h"
#import "ILDSignUpConstants.h"
#import "ILDAPIManager.h"
#import "ILDUtilities.h"
#import "ILDDefines.h"
#import "ILDKeyChainStore.h"
#import "ILDAppDelegate.h"
#import "ILDConsumer.h"
#import "ILDDevice.h"
#import "ILDActivityIndicator.h"
#import "ILDTextField.h"
#import "ILDHomeScreenController.h"
#import "ILDCustomImageView.h"
#import "ILDRandomColors.h"
//#import <GoogleMaps/GoogleMaps.h>
@import MapKit;

#define BOTTOM_SPACING 32
#define MAXIMUM_PHONE_NUMBER_LIMIT 9

#define MANDATORY_ICON_FRAME CGRectMake(0, 0, 20, 15)

#define GENDER_FEMALE               @"Female"
#define GENDER_MALE                 @"Male"


@interface ILDSignUpViewController ()<UITextFieldDelegate,UIScrollViewDelegate,ILDCustomAlertDelegate,UISearchBarDelegate,UITableViewDelegate,UITableViewDataSource,UIGestureRecognizerDelegate,CLLocationManagerDelegate>
{
    ILDAPIManager *manager;
    ILDKeyChainStore *keyChainItem;
    UITapGestureRecognizer *tapGesture;
}
@property (weak, nonatomic) IBOutlet UIButton *tAndcButton;
@property(nonatomic,strong)ILDRandomColors *generator;
@property (weak, nonatomic) IBOutlet UIButton *signUp;
@property (weak, nonatomic) IBOutlet UIView *headerView;

@property (weak, nonatomic) IBOutlet UIScrollView *signUpScrollView;
@property (weak, nonatomic) IBOutlet ILDTextField *emailTextField;
@property (weak, nonatomic) IBOutlet ILDTextField *mobileNumberTextField;
@property (weak, nonatomic) IBOutlet ILDTextField *firstNameTextField;
@property (weak, nonatomic) IBOutlet ILDTextField *lastNameTextField;
@property (weak, nonatomic) IBOutlet ILDTextField *passwordTextField;
@property (weak, nonatomic) IBOutlet ILDTextField *confirmPasswordTextField;
@property (weak, nonatomic) IBOutlet ILDTextField *dateOfBirthTextField;
@property (weak, nonatomic) IBOutlet ILDTextField *cityTextField;
@property (weak, nonatomic) IBOutlet ILDTextField *residenceLocalityTextField;
@property (weak, nonatomic) IBOutlet ILDTextField *collegeTextField;
@property (weak, nonatomic) IBOutlet ILDTextField *jobTextField;
@property (weak, nonatomic) IBOutlet ILDTextField *companyTextField;
@property (weak, nonatomic) IBOutlet ILDTextField *officeLocalityTextField;
@property (weak, nonatomic) IBOutlet ILDTextField *genderTextField;

@property (weak, nonatomic) IBOutlet UIButton *facebookCheckBox;
@property(nonatomic) BOOL isFacebookCheckBoxChecked;
@property (weak, nonatomic) IBOutlet UILabel *termsAndConditionsLabel;
@property (strong, nonatomic) IBOutlet UITapGestureRecognizer *tapGestureRecognizer;
@property (strong, nonatomic) UITextField *currentTextField;

@property (strong, nonatomic) UIActivityIndicatorView *progress;
@property (strong, nonatomic) NSDate *dateOfBirth;
@property (strong, nonatomic) NSString *uuid;
@property (strong, nonatomic) NSString *notificationIdentifier;
@property (strong, nonatomic) NSString *password;
@property (strong, nonatomic) NSString *officeLocality;
@property (strong, nonatomic) NSString *deviceIdentifier;
@property (strong, nonatomic) UIToolbar *keyboardToolBar;

@property (strong, nonatomic) UISearchBar *searchBar;
@property (strong, nonatomic) UIView *searchBackView;
@property (strong, nonatomic) UITableView *searchTableView;

@property (strong, nonatomic) NSArray *searchTableViewArray;
@property(nonatomic,strong) CLLocationManager *locationManager;
@property (nonatomic) BOOL isLocationFetched;
@property (nonatomic) BOOL isCitySelectedByUser;

@property (strong, nonatomic) UITableView *genderTableView;
@property (nonatomic) BOOL isGenderInitiatedScroll;

@property (nonatomic) BOOL isGenderSelected;
@property (nonatomic) BOOL gender;


@property (weak, nonatomic) IBOutlet UIButton *maleButton;
@property (weak, nonatomic) IBOutlet UIButton *femaleButton;

- (void)registerForKeyboardNotifications;
- (UITextField *)findNextTextField:(UITextField*)currentTextField;
- (void)showServerValidationAlerts:(NSDictionary*)response;
- (void)configureTextField:(UITextField *)textField
                 leftImage:(NSString *)leftImageName
                rightImage:(NSString *)rightImageName
           placeHolderText:(NSString *)placeHolderText;


- (IBAction)signUp:(UIButton *)sender;
- (IBAction)goBack:(id)sender;
@end


@implementation ILDSignUpViewController


-(CLLocationManager *)locationManager
{
    if(_locationManager != nil)
    {
        return _locationManager;
    }
    _locationManager = [[CLLocationManager alloc] init];
    _locationManager.delegate = self;
    _locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters;
    return _locationManager;
}

-(void)setGender:(BOOL)gender
{
    _gender = gender;
    if(self.isGenderSelected)
    {
        if(gender){
            [self.maleButton setImage:[UIImage imageNamed:@"select"] forState:UIControlStateNormal];
            [self.femaleButton setImage:[UIImage imageNamed:@"unselect"] forState:UIControlStateNormal];

        }else{
            [self.maleButton setImage:[UIImage imageNamed:@"unselect"] forState:UIControlStateNormal];
            [self.femaleButton setImage:[UIImage imageNamed:@"select"] forState:UIControlStateNormal];
        }
    }
}


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
        // Custom initialization
        
    }
    return self;
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    NSString *version = [[UIDevice currentDevice] systemVersion];
    [CLLocationManager authorizationStatus];
    if([version floatValue] >= 8.0) {
        // Use one or the other, not both. Depending on what you put in info.plist
        //[self.locationManager requestWhenInUseAuthorization];
        [self.locationManager requestAlwaysAuthorization];
        
    }
    [self.locationManager startUpdatingLocation];
    self.isLocationFetched = false;
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.headerView.backgroundColor = [_generator getRandomColorforInt:[_generator getRandomcolorCodeNumber]];
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [[UITextField appearanceWhenContainedIn:[UISearchBar class], nil] setDefaultTextAttributes:@{
                                                                                                 NSFontAttributeName: [UIFont fontWithName:ILDFontCalibri size:20],NSForegroundColorAttributeName:[UIColor blackColor]
                                                                                                 }];

    
    _generator = [[ILDRandomColors alloc]init];
    self.isGenderInitiatedScroll = false;
    self.isCitySelectedByUser = false;
    self.cityTextField.userInteractionEnabled = true;
    [self registerForKeyboardNotifications];
    manager = [ILDAPIManager sharedManager];
    keyChainItem = [ILDKeyChainStore sharedKeyChainStore];
    self.uuid = [ILDUtilities getUUID];
    tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(didTappedBackground:)];

    tapGesture.cancelsTouchesInView = NO;
    self.signUpScrollView.delegate = self;
    [self.emailTextField becomeFirstResponder];
    [self displaySignUpInputFields];
    self.deviceIdentifier = [ILDDevice currentDevice].UUID;
    self.notificationIdentifier = @"";
    
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    [paragraphStyle setAlignment:NSTextAlignmentCenter];
    
    self.isFacebookCheckBoxChecked = true;
    
    [self.tAndcButton setAttributedTitle:[[NSAttributedString alloc] initWithString:@"By tapping Sign Up you agree to \nONE Rewardz's Terms and Conditions" attributes:@{NSParagraphStyleAttributeName:paragraphStyle,NSUnderlineStyleAttributeName: @(NSUnderlineStyleSingle), NSForegroundColorAttributeName : [UIColor colorWithRed:0 green:78.0/255.0 blue:134.0/255.0 alpha:1.0]}] forState:UIControlStateNormal];
    [self createKeyboardView];
    
//#pragma mark - Nihsith's code
//    
//    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(cityTextFieldTapped:)];
//    tap.numberOfTapsRequired = 1;
//    [self.cityTextField addGestureRecognizer:tap];
}

- (void)displaySignUpInputFields
{
    [self configureTextField:self.emailTextField leftImage:@"email" rightImage:@"mandatory_icon" placeHolderText:@"E-mail"];
    [self configureTextField:self.mobileNumberTextField leftImage:@"mobile_number" rightImage:@"mandatory_icon" placeHolderText:@"Mobile Number"];
    [self configureTextField:self.firstNameTextField leftImage:@"first_name" rightImage:@"mandatory_icon" placeHolderText:@"First Name"];
    [self configureTextField:self.lastNameTextField leftImage:@"first_name" rightImage:@"mandatory_icon" placeHolderText:@"Last Name"];
    [self configureTextField:self.passwordTextField leftImage:@"password" rightImage:@"mandatory_icon" placeHolderText:@"Password"];
    [self configureTextField:self.confirmPasswordTextField leftImage:@"confirm_password" rightImage:@"mandatory_icon" placeHolderText:@"Confirm Password"];
    [self configureTextField:self.dateOfBirthTextField leftImage:@"date_of_birth" rightImage:@"mandatory_icon" placeHolderText:@"Date of Birth"];
    [self configureTextField:self.cityTextField leftImage:@"location" rightImage:@"mandatory_icon" placeHolderText:@"City"];
    [self configureTextField:self.residenceLocalityTextField leftImage:@"location" rightImage:nil placeHolderText:@"Residence Locality"];
    [self configureTextField:self.collegeTextField leftImage:@"college" rightImage:nil placeHolderText:@"College"];
    [self configureTextField:self.jobTextField leftImage:@"job_title" rightImage:nil placeHolderText:@"Job"];
    [self configureTextField:self.companyTextField leftImage:@"company" rightImage:nil placeHolderText:@"Company"];
    [self configureTextField:self.officeLocalityTextField leftImage:@"location" rightImage:nil placeHolderText:@"Office Locality"];
    [self configureTextField:self.genderTextField leftImage:@"gender_male" rightImage:nil placeHolderText:GENDER_MALE];
}

- (void)configureTextField:(UITextField *)textField
                 leftImage:(NSString *)leftImageName
                rightImage:(NSString *)rightImageName
           placeHolderText:(NSString *)placeHolderText
{
    if(leftImageName)
    {
        UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 30, 10)];
        UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(8,-5, 22, 22)];
        [imageView setImage:[UIImage imageNamed:leftImageName]];
        [view addSubview:imageView];
        textField.leftView = view;
        textField.leftViewMode = UITextFieldViewModeUnlessEditing;
    }
    if(rightImageName)
    {
        UIView *view = [[UIView alloc] initWithFrame:MANDATORY_ICON_FRAME];
        UIImageView *imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:rightImageName]];
        [view addSubview:imageView];
        textField.rightView = view;
        textField.rightViewMode = UITextFieldViewModeAlways;
    }
    
    /*
    NSMutableAttributedString *attributed = [[NSMutableAttributedString alloc]initWithString:placeHolderText];
    [attributed addAttribute:NSFontAttributeName value:[UIFont fontNamesForFamilyName:ILDFontCalibri] range:NSMakeRange(0, placeHolderText.length)];
    [attributed addAttribute:NSForegroundColorAttributeName value:PLACE_HOLDER_TEXT_COLOR range:NSMakeRange(0, placeHolderText.length)];
     */
    
    
    textField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:placeHolderText
                                                                      attributes:@{NSForegroundColorAttributeName:PLACE_HOLDER_TEXT_COLOR,NSFontAttributeName : [UIFont fontWithName:ILDFontCalibri size:17.0]}];
    textField.font = [UIFont fontWithName:ILDFontCalibri size:18];

    
}

- (void)didTappedBackground:(UITapGestureRecognizer*)tapGestureRecognizer
{
  
    if((self.currentTextField.text.length > 0))
    {
        self.currentTextField.leftViewMode = UITextFieldViewModeNever;
    }

    CGPoint tapLocation = [tapGestureRecognizer locationInView:self.signUpScrollView];
    CGRect buttonRect = self.signUp.frame;
    if(CGRectContainsPoint(buttonRect, tapLocation))
    {
        [self signUp:nil];
    }
    [self.currentTextField resignFirstResponder];
    
    if(CGRectContainsPoint(self.tAndcButton.frame, tapLocation))
    {
        [self tAndCbuttonPressed:(UIButton *) tapGestureRecognizer.view];
    }
    [self hideGenderTable];

}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    [self.signUpScrollView setContentSize:CGSizeMake(CGRectGetWidth(self.signUpScrollView.frame),
                                                     self.tAndcButton.frame.origin.y +
                                                     CGRectGetHeight(self.tAndcButton.frame) + BOTTOM_SPACING)];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)maleButtonPressed:(UIButton *)sender {
    self.isGenderSelected = true;
    self.gender = true;
    
}
- (IBAction)femaleButtonPressed:(UIButton *)sender {
    self.isGenderSelected = true;
    self.gender = false;
}

- (IBAction)goBack:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)createKeyboardView
{
    self.keyboardToolBar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, 50)];
    NSDictionary *barButtonAppearanceDict = @{NSFontAttributeName:[UIFont fontWithName:ILDFontCalibri size:24.0]
                                              ,NSForegroundColorAttributeName :UIColorFromRGB(0xffffff)};
    
    [[UIBarButtonItem appearance] setTitleTextAttributes:barButtonAppearanceDict forState:UIControlStateNormal];
    if([self.keyboardToolBar respondsToSelector:@selector(setBarTintColor:)])
    {
        self.keyboardToolBar.barTintColor = UIColorFromRGB(0x0070c0);
    }
    else
    {
        self.keyboardToolBar.tintColor = UIColorFromRGB(0x0070c0);
    }
    self.keyboardToolBar.barStyle = UIBarStyleBlackTranslucent;
}

- (void)createTopBarForKeyBoard:(UITextField *)textField
{
    if (!self.keyboardToolBar)
    {
        [self createKeyboardView];
    }
    
    NSArray *toolBarItems;
    UIBarButtonItem *next  =  [[UIBarButtonItem alloc] initWithTitle:ILDNext
                                                               style:UIBarButtonItemStyleBordered
                                                              target:self
                                                              action:@selector(nextFieldButtonPressed)];
    UIBarButtonItem *cancel = [[UIBarButtonItem alloc] initWithTitle:ILDCancel
                                                               style:UIBarButtonItemStyleBordered
                                                              target:self action:@selector(cancelButtonPressed)];
    UIBarButtonItem *space = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace
                                                                           target:nil action:nil];
    if(textField.keyboardType == UIKeyboardTypePhonePad || textField == self.dateOfBirthTextField)
    {
        toolBarItems = [[NSArray alloc] initWithObjects: cancel, space, next, nil];
    }
    else
    {
        toolBarItems = [[NSArray alloc] initWithObjects:space,next,nil];
    }
    [self.keyboardToolBar setItems:toolBarItems];
    textField.inputAccessoryView = self.keyboardToolBar;
}

- (void)cancelButtonPressed
{
    [self.currentTextField resignFirstResponder];
}

- (void)nextFieldButtonPressed
{
    UITextField *nextTextField = [self findNextTextField:self.currentTextField];
    [self.currentTextField resignFirstResponder];
    [nextTextField becomeFirstResponder];
}

- (void)displayOTPEnteringAlert
{
    ILDCustomAlert  *alert = [[ILDCustomAlert alloc] initForInputWithTitle:@"Successful!!"
                                                                   message:@"Please check OTP in mobile and please enter here.."
                                                                 inputText:@""
                                                           placeholderText:ILDOTP
                                                                  delegate:self
                                                         cancelButtonTitle:ILDCancel
                                                          otherButtonTitle:ILDOK
                                                                errorAlert:NO];
    alert.keyboardType = ILDKeyboardTypeNumberPad;
    alert.titleTextAllignment=ILDTextAlignmentCenter;
    alert.tag = enterOTPDialog;
    [alert show];
}

- (void)alertView:(ILDCustomAlert *)alertView tappedButtonAtIndex:(NSInteger)buttonIndex
{
    switch (alertView.tag)
    {
        case notEnteredOTPDialog:
            if(buttonIndex == 1)
            {
                [self displayOTPEnteringAlert];
            }
            break;
        case inValidOTPDialog:
            if(buttonIndex == 1)
            {
                [self displayOTPEnteringAlert];
            }
            break;
        case smsVerificationMessageDialog:
            if (buttonIndex == 1)
            {
                [self handleSignUp];
            }
            
        default:
            break;
    }
}

- (void)alertView:(ILDCustomAlert *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex inputText:(NSString *)inputText
{
    if (alertView.tag == enterOTPDialog)
    {
        if(buttonIndex == 1)
        {
            if(inputText == nil || [inputText isEqualToString:@""])
            {
                
                ILDCustomAlert  *alert = [[ILDCustomAlert alloc] initWithTitle:@"Error!"
                                                                       message:@"Please enter OTP!"
                                                                      delegate:self
                                                             cancelButtonTitle:ILDCancel
                                                              otherButtonTitle:ILDOK
                                                                    errorAlert:YES];
                alert.tag = notEnteredOTPDialog;
                [alert show];
            }
            else
            {
                NSNumber *otp = [NSNumber numberWithInteger:[inputText integerValue]];
                [self handleSignUpWithOTP:otp];
            }
        }
    }
}

- (void)handleSignUp
{
    [self.currentTextField resignFirstResponder];
    NSDictionary *parameters = @{ILDSignUpStateKey:@1,ILDEmailIdKey:self.emailTextField.text,ILDPhoneNumberKey:self.mobileNumberTextField.text};
    [manager POST:@"consumers/signup"
       parameters:parameters
          success:^(AFHTTPRequestOperation *operation, id responseObject)
                 {
                     [self handleSignupResponse:responseObject];
                 }
          failure:^(AFHTTPRequestOperation *operation, NSError *error)
                 {
                     NSInteger errorCode = [error code];
                     if (errorCode == NSURLErrorTimedOut ||
                         errorCode == NSURLErrorBadServerResponse ||
                         errorCode == NSURLErrorNetworkConnectionLost ||
                         errorCode == NSURLErrorCannotConnectToHost ||
                         errorCode == NSURLErrorCannotFindHost ||
                         errorCode ==  NSURLErrorResourceUnavailable)
                     {
                         ILDCustomAlert *alert = [[ILDCustomAlert alloc] initWithTitle:@"Oops!"
                                                                               message:@"ONE Rewardz server unreachable! End of the world to follow.."
                                                                     cancelButtonBlock:^{
                                                                         
                                                                         exit(0);
                                                                         
                                                                     } otherButtonBlock:^{
                                                                         [self handleSignUp];
                                                                         
                                                                     } cancelButtonTitle:ILDQuit
                                                                      otherButtonTitle:ILDRetry
                                                                            errorAlert:YES];
                         [alert show];
                     }
                 }
          message:ILDAIMSGConnectingToSqy
clientHandleError:YES
     ];
}

#pragma mark - Build version detection method


- (NSString *) appVersion
{
    return [[NSBundle mainBundle] objectForInfoDictionaryKey: @"CFBundleShortVersionString"];
}

- (NSString *) build
{
    return [[NSBundle mainBundle] objectForInfoDictionaryKey: (NSString *)kCFBundleVersionKey];
}


-(NSString *) versionBuild
{
    NSString * version = [self appVersion];
    NSString * build = [self build];
    
    NSString * versionBuild = [NSString stringWithFormat: @"v%@", version];
    
    if (![version isEqualToString: build]) {
        versionBuild = [NSString stringWithFormat: @"%@(%@)", versionBuild, build];
    }
    
    return versionBuild;
}

#pragma mark - Build version detection methods ended


- (void)handleSignUpWithOTP:(NSNumber*)otp
{
    
    NSString *mobileNumber      = (self.mobileNumberTextField.text)? self.mobileNumberTextField.text:@"";
    NSString *firstName         = (self.firstNameTextField.text)? self.firstNameTextField.text:@"";
    NSString *lastName          = (self.lastNameTextField.text)? self.lastNameTextField.text:@"";
    NSString *email             = (self.emailTextField.text)? self.emailTextField.text:@"";
    self.password               = (self.passwordTextField.text)? self.passwordTextField.text:@"";
    NSString *dateOfBirthText   = (self.dateOfBirthTextField.text)? self.dateOfBirthTextField.text:@"";
    NSString *city              = (self.cityTextField.text)? self.cityTextField.text:@"";
    NSString *residenceLocality = (self.residenceLocalityTextField.text)? self.residenceLocalityTextField.text:@"";
    NSString *college           = (self.collegeTextField.text)? self.collegeTextField.text:@"";
    NSString *job               = (self.jobTextField.text)? self.jobTextField.text:@"";
    NSString *company           = (self.companyTextField.text)? self.companyTextField.text:@"";
    NSString *officeLocality    = (self.officeLocalityTextField.text)? self.officeLocalityTextField.text:@"";
    
    NSNumber *genderNumber = @1;
    if(self.isGenderSelected && !self.gender)
    {
        genderNumber = @0;
    }
    
    NSDictionary *parameters = @{ILDSignUpStateKey:@2,
                                 ILDOtpKey:otp,
                                 ILDFirstNameKey:firstName,
                                 ILDLastNameKey:lastName,
                                 ILDPasswordKey:self.password,
                                 ILDDateOfBirthKey:dateOfBirthText,
                                 ILDCityKey:city,
                                 ILDEmailIdKey:email,
                                 ILDPhoneNumberKey:mobileNumber,
                                 ILDOfficeLocalityKey:officeLocality,
                                 ILDResidenceLocalityKey:residenceLocality,
                                 ILDCompanyKey:company,
                                 ILDCollegeKey:college,
                                 ILDJobKey:job,
                                 ILDSimIdentifierKey:@"IOSApp",
                                 ILDDeviceIdentifierKey:self.deviceIdentifier,
                                 ILDNotificationIdentifierKey:self.notificationIdentifier,
                                 ILDPlatformKey:@2,
                                 ILDVersionname:[self appVersion],
                                 ILDVersionCode:[self build],
                                 ILDGenderKey:genderNumber};
    
    
    [manager POST:@"consumers/signup"
       parameters:parameters
          success:^(AFHTTPRequestOperation *operation, id responseObject)
                 {
                     [self handleSignupResponse:responseObject];
                 }
          failure:^(AFHTTPRequestOperation *operation, NSError *error)
                 {
                     if (error.code == NSURLErrorTimedOut || error.code == NSURLErrorBadServerResponse || error.code == NSURLErrorNetworkConnectionLost || error.code == NSURLErrorCannotConnectToHost || error.code == NSURLErrorCannotFindHost || error.code ==  NSURLErrorResourceUnavailable)
                     {
                         ILDCustomAlert *alert = [[ILDCustomAlert alloc] initWithTitle:@"Oops!"
                                                                               message:@"ONE Rewardz server unreachable! End of the world to follow.."
                                                                     cancelButtonBlock:^{
                                                                         
                                                                         exit(0);
                                                                         
                                                                     } otherButtonBlock:^{
                                                                         
                                                                         [self handleSignUpWithOTP:otp];
                                                                         
                                                                     } cancelButtonTitle:ILDQuit
                                                                      otherButtonTitle:ILDRetry
                                                                            errorAlert:YES];
                         [alert show];
                     }

                 }
          message:ILDAIMSGConnectingToSqy clientHandleError:YES
     ];
}

- (void)handleSignupResponse:(NSDictionary*)response
{
    if ([self isSignUpSuccess:response])
    {
        [self performPostSignupOperationsForResponse:response];
    }
    else
    {
        [self showServerValidationAlerts:response];
    }
}

- (BOOL) isSignUpSuccess:(NSDictionary*) signupResponse
{
    return ([[signupResponse allKeys] containsObject:ILDConsumerIDKey] && [[signupResponse allKeys] containsObject:ILDFirstNameKey]);
}

- (void) performPostSignupOperationsForResponse:(NSDictionary*) signupResponse
{
    NSString *consumerId=[signupResponse valueForKey:ILDConsumerIDKey];

    [ILDConsumer signUpWithDeviceId:[ILDDevice currentDevice]
                         consumerId:consumerId
                           password:self.password];
    ILDConsumer *consumer = [ILDConsumer currentConsumer];
    [consumer updateUserAndRegisterForNotification:true];
    consumer.completionHandler=^{
        
        UIStoryboard *mainStoryBoard = [UIStoryboard storyboardWithName:ILDMainStoryboard bundle:nil];
        UINavigationController *viewController = [mainStoryBoard instantiateViewControllerWithIdentifier:@"HomeScreen"];
        self.view.window.rootViewController = viewController;
        ((ILDHomeScreenController *)[viewController.viewControllers objectAtIndex:0]).isFacebookEnabled = self.isFacebookCheckBoxChecked;
        ((ILDHomeScreenController *)[viewController.viewControllers objectAtIndex:0]).isLaunchedFromSignUp = true;
    };
}


- (void)showServerValidationAlerts:(NSDictionary*)response
{
    NSUInteger errorCode = [[response valueForKey:ILDErrorCodeKey]integerValue];
    NSString *errorTitle = nil;
    NSString *errorMessage = nil;
    BOOL actionNeeded = NO;
    
    switch (errorCode)
    {
        case 100:
        {
            errorTitle = ILDSignUpFailedTitle;
            errorMessage = @"Looks like you missed something !";
            break;
        }
        case 101:
        {
            errorTitle = ILDSignUpFailedTitle;
            errorMessage = @"Multiple personalities huh?! We already have this email id registered!";
            break;
        }
        case 102:
        {
            errorTitle = ILDSignUpFailedTitle;
            errorMessage = @"OTP could not be sent via SMS !";
            break;
        }
        case 103:
        {
            actionNeeded = YES;
            [self displayOTPEnteringAlert];
            break;
        }
        case 104:
        {
            actionNeeded = YES;
            ILDCustomAlert *alertView = [[ILDCustomAlert alloc]initWithTitle:ILDSignUpFailedTitle
                                                                     message:@"Invalid OTP!"
                                                                    delegate:self
                                                           cancelButtonTitle:ILDCancel
                                                            otherButtonTitle:ILDRetry
                                                                  errorAlert:YES];
            alertView.tag = inValidOTPDialog;
            [alertView show];
            break;
        }
        case 105:
        {
            errorTitle = ILDSignUpFailedTitle;
            errorMessage = @"Your mobile number verification failed!!";
            break;
        }
        case 106:
        {
            errorTitle = @"Oops!";
            errorMessage = @"ONE Rewardz server unreachable! End of the world to follow..";
            break;
        }
        case 107:
        {
            errorTitle = ILDSignUpFailedTitle;
            errorMessage = @"Email Id not registered.";
            break;
        }
        case 117:
        {
            errorTitle = @"Oops!";
            errorMessage = @"ONE Rewardz server unreachable! End of the world to follow..";
            break;
        }
           
        case 118:
        {
            errorTitle = ILDSignUpFailedTitle;
            errorMessage = @"Invalid date format";
            break;
        }
        case 119:
        {
            errorTitle = ILDSignUpFailedTitle;
            errorMessage = @"Grow up kid.. Try us again when you're 10..";
            break;
        }
        case 124:
        {
            errorTitle = ILDSignUpFailedTitle;
            errorMessage = @"What kind of mobile number is that?";
            break;
        }
        case 123:
        {
            errorTitle = ILDSignUpFailedTitle;
            errorMessage = @"Email Id not sent";

            break;
        }
        case 125:
        {
            errorTitle = ILDSignUpFailedTitle;
            errorMessage = @"Grow up kid.. Try us again when you're 10..";

            break;
        }
        default:
            break;
    }

    if (!actionNeeded)
    {
        ILDCustomAlert *alertView = [[ILDCustomAlert alloc] initWithTitle:errorTitle
                                                                  message:errorMessage
                                                                 delegate:self
                                                        cancelButtonTitle:ILDOK
                                                         otherButtonTitle:nil
                                                               errorAlert:YES];
        [alertView show];
    }
}


- (void)showValidationAlerts:(validationStatus)status
{
    NSString *errorTitle = nil;
    NSString *errorMessage = nil;
    
    switch (status)
    {
        case missedInput:
            errorTitle = ILDSignUpFailedTitle;
            errorMessage = @"Looks like you missed something !";
            break;
            
        case invalidPasswordLength:
            errorTitle = ILDSignUpFailedTitle;
            errorMessage = @"Password is too short!\nNeed at least 6 characters!!";
            break;

        case passwordMismatch:
            errorTitle = ILDSignUpFailedTitle;
            errorMessage = @"Feeling sleepy? Passwords don't match!";
            break;
            
        case invalidEmailId:
            errorTitle = ILDSignUpFailedTitle;
            errorMessage = @"That's a weird email address!";
            break;
            
        case invalidAge:
        {
            DateStatus status=[ILDUtilities validateDateOfBirth:self.dateOfBirth];
            switch (status)
            {
                case futureDate:
                {
                    errorTitle = ILDSignUpFailedTitle;
                    errorMessage = @"Invalid date of Birth";
                    break;
                }
                    
                case earlierDate:
                {
                    errorTitle = ILDSignUpFailedTitle;
                    errorMessage = @"Grow up kid.. Try us again when you're 10..";
                    break;
                }
                    
                default:
                    break;
            }
            break;
            
        }
            
        case invalidPhoneNo:
            errorTitle = ILDSignUpFailedTitle;
            errorMessage = @"What kind of mobile number is that?";
        
        case unselectedGender:
            errorTitle = ILDSignUpFailedTitle;
            errorMessage = @"Sex please!\n(the gender one...)";
            
        default:
            break;
    }
    ILDCustomAlert *alert = [[ILDCustomAlert alloc] initWithTitle:errorTitle
                                                          message:errorMessage
                                                         delegate:self
                                                cancelButtonTitle:ILDOK
                                                 otherButtonTitle:nil
                                                       errorAlert:YES];
    [alert show];
}

- (IBAction)signUp:(UIButton *)sender
{
    [self.currentTextField resignFirstResponder];
     validationStatus status = [self validateFields];
     if(status == success)
     {
         ILDCustomAlert *alert = [[ILDCustomAlert alloc]initWithTitle:nil
                                                              message:@"An SMS with verification code will be sent to your mobile number to activate your account"
                                                             delegate:self
                                                    cancelButtonTitle:ILDCancel
                                                    otherButtonTitle:ILDOK errorAlert:NO];
         alert.tag = smsVerificationMessageDialog;
         [alert show];
     }else
     {
         [self showValidationAlerts:status];
     }
}


- (UITextField *)findNextTextField:(UITextField *)currentTextField
{
    if (currentTextField == self.emailTextField) {
        return self.mobileNumberTextField;
    }
    
    NSUInteger nextIndex = 0;
    NSUInteger currentIndex;
    if(currentTextField.text.length > 0)
    {
        currentTextField.leftViewMode = UITextFieldViewModeNever;
    }
    for(id textField in self.signUpScrollView.subviews)
    {
        if ((textField == currentTextField) && ([textField isKindOfClass:[UITextField class]]))
        {
            currentIndex = [self.signUpScrollView.subviews indexOfObject:textField];
            nextIndex = currentIndex+1;
            break;
        }
    }

    return [self.signUpScrollView.subviews objectAtIndex:nextIndex];
}

- (validationStatus)validateFields
{
    validationStatus status;

    if(self.emailTextField.text.length == 0 || self.mobileNumberTextField.text.length == 0 || self.firstNameTextField.text.length == 0 || self.lastNameTextField.text.length == 0 || self.passwordTextField.text.length == 0 || self.confirmPasswordTextField.text.length == 0 || self.dateOfBirthTextField.text.length == 0 || self.cityTextField.text.length == 0)
    {
        status = missedInput;
    }
    else
    {
        if(self.passwordTextField.text.length < 6)
        {
            status = invalidPasswordLength;
        }
        else
        {
            if(![self.passwordTextField.text isEqualToString:self.confirmPasswordTextField.text])
            {
                status = passwordMismatch;
            }
            else
            {
                if(![ILDUtilities isValidEmailId:self.emailTextField.text])
                {
                    status = invalidEmailId;
                }
                else
                {
                    if(self.mobileNumberTextField.text.length < 10)
                    {
                        status = invalidPhoneNo;
                    }
                    else
                    {
                        if(![ILDUtilities isValidAge:self.dateOfBirth])
                        {
                            status = invalidAge;
                        }
                        else
                        {
                            if(!self.isGenderSelected)
                            {
                                status = unselectedGender;
                            }else{
                                status = success;
                            }
                        }
                    }
                }
            }
        }
    }
    return status;
}

#pragma mark - UITextField delegate methods


- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    if(textField == self.cityTextField || textField == self.genderTextField)
    {
        if([self.emailTextField isFirstResponder])
        {
            [self.emailTextField resignFirstResponder];
        }else if([self.mobileNumberTextField isFirstResponder])
        {
            [self.mobileNumberTextField resignFirstResponder];
        }else if([self.firstNameTextField isFirstResponder])
        {
            [self.firstNameTextField resignFirstResponder];
        }else if([self.lastNameTextField isFirstResponder])
        {
            [self.lastNameTextField resignFirstResponder];
        }else if([self.passwordTextField isFirstResponder])
        {
            [self.passwordTextField resignFirstResponder];
        }else if([self.confirmPasswordTextField isFirstResponder])
        {
            [self.confirmPasswordTextField resignFirstResponder];
        }else if([self.dateOfBirthTextField isFirstResponder])
        {
            [self.dateOfBirthTextField resignFirstResponder];
        }else if([self.residenceLocalityTextField isFirstResponder])
        {
            [self.residenceLocalityTextField resignFirstResponder];
        }else if([self.collegeTextField isFirstResponder])
        {
            [self.collegeTextField resignFirstResponder];
        }else if([self.jobTextField isFirstResponder])
        {
            [self.jobTextField resignFirstResponder];
        }else if([self.companyTextField isFirstResponder])
        {
            [self.companyTextField resignFirstResponder];
        }else if([self.officeLocalityTextField isFirstResponder])
        {
            [self.officeLocalityTextField resignFirstResponder];
        }
        if(textField == self.cityTextField){
            [self cityTextFieldTapped:nil];
        }else if(textField == self.genderTextField)
        {
            [self genderTextFieldTapped:nil];
        }
        return false;
    }else{
        [self hideGenderTable];
    }
    return true;
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if(textField.text.length > 0)
    {
        textField.leftViewMode = UITextFieldViewModeNever;
    }
    [textField resignFirstResponder];
    textField.inputAccessoryView = nil;
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    if(textField.text.length > 0)
    {
        textField.leftViewMode = UITextFieldViewModeNever;
    }
    textField.inputAccessoryView = nil;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    // allow backspace
    if (!string.length)
    {
        textField.leftViewMode = UITextFieldViewModeUnlessEditing;
        return YES;
    }
    // Prevent invalid character input, if keyboard is numberpad
    if (textField.keyboardType == UIKeyboardTypeNumberPad)
    {
        if ([string rangeOfCharacterFromSet:[[NSCharacterSet decimalDigitCharacterSet] invertedSet]].location != NSNotFound)
        {
            return NO;
        }
    }
    if(textField == self.firstNameTextField || textField == self.lastNameTextField ||
       textField == self.dateOfBirthTextField ||textField == self.cityTextField ||
       textField == self.residenceLocalityTextField || textField == self.collegeTextField ||
       textField == self.jobTextField || textField == self.collegeTextField ||
       textField == self.companyTextField || textField == self.officeLocalityTextField)
    {
        NSMutableCharacterSet *allowedCharacters = [NSMutableCharacterSet characterSetWithCharactersInString:@" -_.'"];
        NSCharacterSet *alphaNumeric = [NSCharacterSet alphanumericCharacterSet];
        [allowedCharacters formUnionWithCharacterSet:alphaNumeric];
        if ([string rangeOfCharacterFromSet:[allowedCharacters invertedSet]].location != NSNotFound)
        {
            return NO;
        }
    }
    if(textField == self.mobileNumberTextField)
    {
        if(range.length + range.location > 9)
        {
            return NO;
        }
        NSMutableCharacterSet *allowedCharacters = [NSMutableCharacterSet characterSetWithCharactersInString:@" -_.'"];
        NSCharacterSet *decimal = [NSCharacterSet decimalDigitCharacterSet];
        [allowedCharacters formUnionWithCharacterSet:decimal];
        if ([string rangeOfCharacterFromSet:[allowedCharacters invertedSet]].location != NSNotFound)
        {
            return NO;
        }
        
    }
    return YES;
}

#pragma mark UIKeyboard handling

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    self.currentTextField = textField;
    [self createTopBarForKeyBoard:textField];

    if(textField == self.dateOfBirthTextField)
    {
        UIDatePicker *datePicker = [[UIDatePicker alloc]init];
        datePicker.datePickerMode = UIDatePickerModeDate;
        [datePicker setDate:[NSDate date]];
        [datePicker addTarget:self action:@selector(updateDateField:) forControlEvents:UIControlEventValueChanged];
        
        self.dateOfBirthTextField.inputView = datePicker;
        self.dateOfBirthTextField.text = [self formatDate:datePicker.date];
        self.dateOfBirth = datePicker.date;
    }
}

// Called when the date picker changes.
- (void)updateDateField:(id)sender
{
   UIDatePicker *picker = (UIDatePicker*)self.dateOfBirthTextField.inputView;
    self.dateOfBirthTextField.text = [self formatDate:picker.date];
    self.dateOfBirth = picker.date;
}

// Formats the date chosen with the date picker.
- (NSString *)formatDate:(NSDate *)date
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateStyle:NSDateFormatterShortStyle];
    [dateFormatter setDateFormat:@"yyyy'-'MM'-'dd"];
    NSString *formattedDate = [dateFormatter stringFromDate:date];
    return formattedDate;
}

- (void)registerForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardWillShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
}

// Called when the UIKeyboardDidShowNotification is sent.
- (void)keyboardWasShown:(NSNotification*)aNotification
{
    if(self.searchBar != nil && [self.searchBar isFirstResponder])
    {
        NSDictionary* info = [aNotification userInfo];
        CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;

        self.searchTableView.frame = CGRectMake(self.searchTableView.frame.origin.x, self.searchTableView.frame.origin.y, self.searchTableView.frame.size.width, self.searchTableView.frame.size.height - kbSize.height);
    }else
    {
        NSDictionary* info = [aNotification userInfo];
        CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
        
        UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, kbSize.height + 50, 0.0);
        self.signUpScrollView.contentInset = contentInsets;
        self.signUpScrollView.scrollIndicatorInsets = contentInsets;
        
        // If active text field is hidden by keyboard, scroll it so it's visible
        // Your app might not need or want this behavior.
        CGRect aRect = self.view.frame;
        aRect.size.height -= kbSize.height + 50;
        if (!CGRectContainsPoint(aRect, self.currentTextField.frame.origin) )
        {
            [self.signUpScrollView scrollRectToVisible:self.currentTextField.frame animated:YES];
        }
        [self.signUpScrollView addGestureRecognizer:tapGesture];
    }
}

// Called when the UIKeyboardWillHideNotification is sent
- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
    if(self.searchBar != nil && [self.searchBar isFirstResponder])
    {
        NSDictionary* info = [aNotification userInfo];
        CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
        
        self.searchTableView.frame = CGRectMake(self.searchTableView.frame.origin.x, self.searchTableView.frame.origin.y, self.searchTableView.frame.size.width, self.searchTableView.frame.size.height + kbSize.height);
    }else
    {
        UIEdgeInsets contentInsets = UIEdgeInsetsZero;
        self.signUpScrollView.contentInset = contentInsets;
        self.signUpScrollView.scrollIndicatorInsets = contentInsets;
        [self.signUpScrollView removeGestureRecognizer:tapGesture];
    }
}

#pragma mark - Code by nishith

-(void)setIsFacebookCheckBoxChecked:(BOOL)isFacebookCheckBoxChecked
{
    _isFacebookCheckBoxChecked = isFacebookCheckBoxChecked;
    if(isFacebookCheckBoxChecked){
        [self.facebookCheckBox setImage:[UIImage imageNamed:@"sharechecked"] forState:UIControlStateNormal];
    }else
    {
        [self.facebookCheckBox setImage:[UIImage imageNamed:@"shareUnchecked"] forState:UIControlStateNormal];
    }
}
- (IBAction)tAndCbuttonPressed:(UIButton *)sender {
    [self performSegueWithIdentifier:@"tandcsegue" sender:sender];
}

- (IBAction)facebookCheckboxButtonPressed:(UIButton *)sender {
    self.isFacebookCheckBoxChecked = !self.isFacebookCheckBoxChecked;
}




#pragma mark - City search code

-(NSArray *)getAllCities
{
    return @[@"Mumbai",@"Delhi", @"Bangalore",@"Hyderabad",@"Ahmedabad",@"Chennai",@"Kolkata",@"Surat",@"Pune",@"Jaipur",@"Lucknow",@"Kanpur",@"Nagpur",@"Indore",@"Thane",@"Bhopal",@"Visakhapatnam",@"Pimpri-Chinchwad",@"Patna",@"Vadodara",@"Ghaziabad",@"Ludhiana",@"Agra",@"Nashik",@"Faridabad",@"Meerut",@"Rajkot",@"Kalyan-Dombivali",@"Vasai-Virar",@"Varanasi",@"Srinagar",@"Aurangabad",@"Dhanbad",@"Amritsar",@"Navi Mumbai",@"Allahabad",@"Ranchi",@"Howrah",@"Coimbatore",@"Jabalpur",@"Gwalior",@"Vijayawada",@"Jodhpur",@"Madurai",@"Raipur",@"Kota",@"Guwahati",@"Chandigarh",@"Solapur",@"Hubballi-Dharwad",@"Bareilly",@"Moradabad",@"Mysore",@"Gurgaon",@"Aligarh",@"Jalandhar",@"Tiruchirappalli",@"Bhubaneswar",@"Salem",@"Mira-Bhayandar",@"Trivandrum(Thiruvananthapuram)",@"Bhiwandi",@"Saharanpur",@"Gorakhpur",@"Guntur",@"Bikaner",@"Amravati",@"Noida",@"Jamshedpur",@"Bhilai",@"Warangal",@"Mangalore",@"Cuttack",@"Firozabad",@"Kochi (Cochin)",@"Bhavnagar",@"Dehradun",@"Durgapur",@"Asansol",@"Nanded",@"Kolhapur",@"Ajmer",@"Gulbarga",@"Jamnagar",@"Ujjain",@"Loni",@"Siliguri",@"Jhansi",@"Ulhasnagar",@"Nellore",@"Jammu",@"Sangli-Miraj and Kupwad",@"Belgaum",@"Ambattur",@"Tirunelveli",@"Malegaon",@"Gaya",@"Jalgaon",@"Udaipur",@"Maheshtala",@"Tirupur",@"Davanagere",@"Kozhikode (Calicut)",@"Akola",@"Kurnool",@"Rajpur Sonarpur",@"Bokaro",@"South Dumdum",@"Bellary",@"Patiala",@"Gopalpur",@"Agartala",@"Bhagalpur",@"Muzaffarnagar",@"Bhatpara",@"Panihati",@"Latur",@"Dhule",@"Rohtak",@"Korba",@"Bhilwara",@"Brahmapur",@"Muzaffarpur",@"Ahmednagar",@"Mathura",@"Kollam (Quilon)",@"Avadi",@"Rajahmundry",@"Kadapa",@"Kamarhati",@"Bilaspur",@"Shahjahanpur",@"Bijapur",@"Rampur",@"Shivamogga(Shimoga)",@"Chandrapur",@"Junagadh",@"Thrissur",@"Alwar",@"Bardhaman",@"Kulti",@"Kakinada",@"Nizamabad",@"Parbhani",@"Tumkur",@"Hisar",@"Ozhukarai",@"Bihar",@"Panipat",@"Darbhanga",@"Bally",@"Aizawl",@"Dewas",@"Ichalkaranji",@"Tirupati",@"Karnal",@"Bathinda",@"Jalna",@"Barasat",@"Kirari Suleman Nagar",@"Purnia",@"Satna",@"Mau",@"Sonipat",@"Farrukhabad",@"Sagar",@"Rourkela",@"Durg",@"Imphal",@"Ratlam",@"Hapur",@"Anantapur",@"Arrah",@"Karimnagar",@"Etawah",@"Ambernath",@"North Dumdum",@"Bharatpur",@"Begusarai",@"New Delhi",@"Gandhidham",@"Baranagar",@"Tiruvottiyur",@"Puducherry",@"Sikar",@"Thoothukudi",@"Rewa",@"Mirzapur",@"Raichur",@"Pali",@"Ramagundam",@"Vizianagaram",@"Katihar",@"Haridwar",@"Sri Ganganagar",@"Karawal Nagar",@"Nagercoil",@"Mango",@"Bulandshahr",@"Thanjavur",@"Sangrur",@"Abohar",@"Mahad",@"Nawanshahar"];
}


-(void)searchBackgroundViewTapped:(UITapGestureRecognizer *) tap
{
    [self hideTable];
}

-(void)cityTextFieldTapped:(UITapGestureRecognizer *) sender
{
    if(self.searchBackView == nil)
    {
        self.searchBackView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
        self.searchBackView.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.7];
        self.searchBackView.userInteractionEnabled = true;
        [self.view addSubview:self.searchBackView];
    }
    if(self.searchBar == nil)
    {
        self.searchBar = [[UISearchBar alloc]initWithFrame:[self.view convertRect:self.cityTextField.frame fromView:self.signUpScrollView]];
        self.searchBar.delegate = self;
        self.searchBar.barTintColor = [UIColor whiteColor];
        self.searchBar.tintColor = [UIColor colorWithRed:76.0/255.0 green:155.0/255.0 blue:211.0/255.0 alpha:1.0];
        self.searchBar.layer.cornerRadius = 3.0;
        self.searchBar.layer.masksToBounds = true;
        self.searchBar.layer.borderWidth = 2.5;
        self.searchBar.layer.borderColor = [UIColor colorWithRed:76.0/255.0 green:155.0/255.0 blue:211.0/255.0 alpha:1.0].CGColor;
        self.searchBar.returnKeyType = UIReturnKeyDone;
        [self.searchBackView addSubview:self.searchBar];
        
        //[self.searchBar becomeFirstResponder];
    }
//    
//    [UIView beginAnimations:nil context:NULL];
//    [UIView setAnimationDuration:0.5];
//    [UIView setAnimationCurve:UIViewAnimationCurveEaseIn];
//    self.searchBar.center = CGPointMake(self.searchBar.center.x, self.headerView.frame.size.height + self.searchBar.frame.size.height/2);
//    [UIView commitAnimations];

    
    [UIView animateWithDuration:0.5
                          delay:0.25
         usingSpringWithDamping:0.9
          initialSpringVelocity:0.3
                        options:UIViewAnimationOptionCurveEaseIn
                     animations:^{
                         self.searchBar.center = CGPointMake(self.searchBar.center.x, self.headerView.frame.size.height + self.searchBar.frame.size.height/2);
                     } completion:^(BOOL finished) {
                         [self showTableView];
                     }];
    
    
    
//    CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:@"position"];
//    [animation setFromValue:[NSValue valueWithCGPoint:self.searchBar.center]];
//    [animation setToValue:[NSValue valueWithCGPoint:CGPointMake(self.searchBar.center.x, self.headerView.frame.size.height + self.searchBar.frame.size.height/2)]];
//    [animation setDuration:0.5];
//    [self.searchBar.layer addAnimation:animation forKey:@"position"];
    
}

-(void)showTableView
{
    if(self.searchTableViewArray == nil)
    {
        self.searchTableViewArray = [[NSArray alloc]initWithArray:[self getAllCities]];
    }
    if(self.searchTableView == nil)
    {
        self.searchTableView = [[UITableView alloc]initWithFrame:CGRectMake(self.searchBar.frame.origin.x, self.searchBar.frame.origin.y + self.searchBar.frame.size.height, self.searchBar.frame.size.width,0) style:UITableViewStylePlain];
        self.searchTableView.delegate = self;
        self.searchTableView.dataSource = self;
        self.searchTableView.layer.cornerRadius = 3.0;
        self.searchTableView.layer.masksToBounds = true;
        self.searchTableView.layer.borderWidth = 2.5;
        self.searchTableView.layer.borderColor = [UIColor colorWithRed:76.0/255.0 green:155.0/255.0 blue:211.0/255.0 alpha:1.0].CGColor;
        [self.searchBackView addSubview:self.searchTableView];
    }
    [UIView animateWithDuration:0.25
                          delay:0.0
         usingSpringWithDamping:0.9
          initialSpringVelocity:0.3
                        options:UIViewAnimationOptionCurveEaseIn
                     animations:^{
                         self.searchTableView.frame =  CGRectMake(self.searchBar.frame.origin.x, self.searchBar.frame.origin.y + self.searchBar.frame.size.height, self.searchBar.frame.size.width,self.searchBackView.frame.size.height - self.searchBar.frame.origin.y - self.searchBar.frame.size.height);
                     } completion:^(BOOL finished) {
                         [self.searchBar becomeFirstResponder];
                         self.searchBackView.userInteractionEnabled = true;
                     }];
}

-(void)hideTable
{
    [UIView animateWithDuration:0.25
                          delay:0.0
         usingSpringWithDamping:0.9
          initialSpringVelocity:0.3
                        options:UIViewAnimationOptionCurveEaseIn
                     animations:^{
                         self.searchTableView.frame = CGRectMake(self.searchBar.frame.origin.x, self.searchBar.frame.origin.y + self.searchBar.frame.size.height, self.searchBar.frame.size.width,0);
                     } completion:^(BOOL finished) {
                         [self hideSearchBar];
                     }];
}

-(void)hideSearchBar
{
    [UIView animateWithDuration:0.5
                          delay:0.25
         usingSpringWithDamping:0.9
          initialSpringVelocity:0.3
                        options:UIViewAnimationOptionCurveEaseIn
                     animations:^{
                         self.searchBar.frame = [self.view convertRect:self.cityTextField.frame fromView:self.signUpScrollView];
                     } completion:^(BOOL finished) {
                         if(self.searchBar != nil)
                         {
                             [self.searchBar removeFromSuperview];
                             self.searchBar = nil;
                         }
                         if(self.searchTableView != nil)
                         {
                             [self.searchTableView removeFromSuperview];
                             self.searchTableView = nil;
                         }
                         if(self.searchTableViewArray != nil)
                         {
                             self.searchTableViewArray = nil;
                         }
                         if(self.searchBackView != nil)
                         {
                             [self.searchBackView removeFromSuperview];
                             self.searchBackView = nil;
                         }
                     }];
}


-(void)genderTextFieldTapped:(UITapGestureRecognizer *) sender
{
    if (self.signUpScrollView.contentOffset.y >= (self.signUpScrollView.contentSize.height - self.signUpScrollView.frame.size.height)) {
        //reach bottom
        [self showGenderTableView];
    }else{
        self.isGenderInitiatedScroll = true;
        [self.signUpScrollView setContentOffset:CGPointMake(0, self.signUpScrollView.contentSize.height  - self.signUpScrollView.bounds.size.height) animated:true];
    }
    //self.signUpScrollView.scrollEnabled = false;
}

-(void)showGenderTableView
{
    if(self.genderTableView == nil)
    {
        CGRect genderTextFieldFrame = self.genderTextField.frame;//[self.view convertRect:self.genderTextField.frame fromView:self.signUpScrollView];
        self.genderTableView = [[UITableView alloc]initWithFrame:CGRectMake(genderTextFieldFrame.origin.x,genderTextFieldFrame.origin.y + genderTextFieldFrame.size.height,genderTextFieldFrame.size.width,0) style:UITableViewStylePlain];
        //self.genderTableView.scrollEnabled = false;
        self.genderTableView.delegate = self;
        self.genderTableView.dataSource = self;
        self.genderTableView.layer.cornerRadius = 3.0;
        self.genderTableView.layer.masksToBounds = true;
        self.genderTableView.layer.borderWidth = 2.5;
        self.genderTableView.layer.borderColor = [UIColor colorWithRed:76.0/255.0 green:155.0/255.0 blue:211.0/255.0 alpha:1.0].CGColor;
        [self.signUpScrollView addSubview:self.genderTableView];
        [UIView animateWithDuration:0.5
                              delay:0.25
             usingSpringWithDamping:0.9
              initialSpringVelocity:0.3
                            options:UIViewAnimationOptionCurveEaseIn
                         animations:^{
                             self.genderTableView.frame = CGRectMake(genderTextFieldFrame.origin.x,genderTextFieldFrame.origin.y + genderTextFieldFrame.size.height,genderTextFieldFrame.size.width,44*2);
                         } completion:^(BOOL finished) {
                             
                         }];
    }
}

-(void)hideGenderTable
{
    if(self.genderTextField != nil)
    {
        [UIView animateWithDuration:0.5
                              delay:0.25
             usingSpringWithDamping:0.9
              initialSpringVelocity:0.3
                            options:UIViewAnimationOptionCurveEaseIn
                         animations:^{
                             self.genderTableView.frame = CGRectMake(self.genderTextField.frame.origin.x,self.genderTextField.frame.origin.y + self.genderTextField.frame.size.height,self.genderTextField.frame.size.width,0);
                         } completion:^(BOOL finished) {
                             if(self.genderTableView != nil)
                             {
                                 [self.genderTableView removeFromSuperview];
                                 self.genderTableView = nil;
                             }
                         }];
    }
}

#pragma mark - UIScrollview delegate methods

-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    
}



-(void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView
{
    if(self.isGenderInitiatedScroll)
    {
        self.isGenderInitiatedScroll = false;
        [self showGenderTableView];
    }
}

#pragma mark - UITableView Datasource methods
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(tableView == self.searchTableView){
        return self.searchTableViewArray.count;
    }else if (tableView == self.genderTableView)
    {
        return 2;
    }
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(tableView == self.searchTableView){
        UITableViewCell *cell;
        cell = [tableView dequeueReusableCellWithIdentifier:@"citycell"];
        if(cell == nil)
        {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"citycell"];
        }
        cell.textLabel.text = [self.searchTableViewArray objectAtIndex:indexPath.row];
        cell.textLabel.font = [UIFont fontWithName:ILDFontCalibri size:17];
        return cell;
    }else if(tableView == self.genderTableView)
    {
        UITableViewCell *cell;
        cell = [tableView dequeueReusableCellWithIdentifier:@"genderCell"];
        if(cell == nil)
        {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"genderCell"];
        }
        switch (indexPath.row) {
            case 0:
                cell.textLabel.text = GENDER_MALE;
                cell.imageView.image = [UIImage imageNamed:@"gender_male"];
                break;
            case 1:
                cell.textLabel.text = GENDER_FEMALE;
                cell.imageView.image = [UIImage imageNamed:@"gender_female"];
                break;
            default:
                break;
        }
        return cell;
    }
    return [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"dummycell"];
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(tableView == self.searchTableView){
        [tableView deselectRowAtIndexPath:indexPath animated:true];
        self.searchBar.text = [self.searchTableViewArray objectAtIndex:indexPath.row];
        [self.searchBar resignFirstResponder];
        self.cityTextField.leftView = nil;
        self.cityTextField.text = [self.searchTableViewArray objectAtIndex:indexPath.row];
        [self hideTable];
    }else if(tableView == self.genderTableView){
        self.signUpScrollView.scrollEnabled = true;
        switch (indexPath.row) {
            case 0:
                [self configureTextField:self.genderTextField leftImage:@"gender_male" rightImage:nil placeHolderText:GENDER_MALE];
                break;
            case 1:
                [self configureTextField:self.genderTextField leftImage:@"gender_female" rightImage:nil placeHolderText:GENDER_FEMALE];
                break;
            default:
                break;
        }
        [self hideGenderTable];
    }
}
#pragma mark - UISearchBar delegate methods

- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar
{
    
}
- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    self.searchTableViewArray = nil;
    if(searchText.length > 0){
        NSPredicate *searchSearch = [NSPredicate predicateWithFormat:@"self BEGINSWITH[cd] %@", searchText];
        self.searchTableViewArray = [[self getAllCities] filteredArrayUsingPredicate:searchSearch];
    }else{
        self.searchTableViewArray = [self getAllCities];
    }
    [self.searchTableView reloadData];

}
- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    /*[self.searchBar resignFirstResponder];
    self.cityTextField.leftView = nil;
    self.cityTextField.text = self.searchBar.text;
    self.isCitySelectedByUser = true;
    [self hideTable];*/
}


#pragma mark - Touch Event Handler methods

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    UITouch *touch = [touches anyObject];
    if(touch.view == self.searchBackView)
    {
        [self.searchBar resignFirstResponder];
        [self hideTable];
    }
}

#pragma mark - CLLocationManager Deleagte

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    [self.locationManager stopUpdatingLocation];
    if(!self.isLocationFetched && !self.isCitySelectedByUser){
        self.isLocationFetched = true;
        CLLocation *location = [locations lastObject];
        CLGeocoder *geocoder = [[CLGeocoder alloc] init];
        [geocoder reverseGeocodeLocation:location completionHandler:
         ^(NSArray* placemarks, NSError* error){
             if(placemarks.count > 0 && [[placemarks objectAtIndex:0] isKindOfClass:[CLPlacemark class]]){
                 self.cityTextField.leftView = nil;
                 NSString *city = ((CLPlacemark *)[placemarks objectAtIndex:0]).locality;
                 [[self getAllCities] enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
                         if([obj isKindOfClass:[NSString class]])
                         {
                             if([(NSString *)obj caseInsensitiveCompare:city] == NSOrderedSame)
                             {
                                 dispatch_async(dispatch_get_main_queue(), ^{
                                     self.cityTextField.text = ((CLPlacemark *)[placemarks objectAtIndex:0]).locality;
                                     *stop = true;
                                 });

                             }
                         }
                 }];
             }
             //self.cityTextField.text = responce.firstResult.locality;
             self.locationManager.delegate = nil;
             self.locationManager = nil;
             }];
        
//        [[GMSGeocoder geocoder] reverseGeocodeCoordinate:location.coordinate completionHandler:^(GMSReverseGeocodeResponse *responce, NSError *error) {
//            self.cityTextField.leftView = nil;
//            self.cityTextField.text = responce.firstResult.locality;
//            self.locationManager.delegate = nil;
//            self.locationManager = nil;
//        }];
    }
}


@end
